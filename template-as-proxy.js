import {Blaze} from 'meteor/blaze';
import Templating from "meteor/templating";

const oldTemplate = Templating.Template;

const dummy = new Templating.Template("Template.dummy", () => {});
Templating.Template = function Template(name, viewFunction) {
  oldTemplate.call(this, name, viewFunction);
  Templating.Template.proxies[name] = this;
  return new Proxy(dummy, {
    get(target, key) {
      return Templating.Template.proxies[name][key];
    },
    set(target, key, value) {
      Templating.Template.proxies[name][key] = value;
      return Templating.Template.proxies[name][key];
    }
  });
};

Object.defineProperty(Templating.Template, "_currentTemplateInstanceFunc", {
  get: oldTemplate.__lookupGetter__("_currentTemplateInstanceFunc")
});
Object.keys(oldTemplate).forEach((k) => {
  Templating.Template[k] = oldTemplate[k];
});
Templating.Template.proxies = {};
Templating.Template.prototype = oldTemplate.prototype;

global.Template = Package['templating-runtime'].Template = Blaze.Template = Templating.Template;
