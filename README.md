A simple pacakge to force Blaze Templates to use a proxy. This will allow qualia:reval to hot replace templates and re-render a smaller area of the page, which is good for large, complex UI's where you may only need a small component to be re-rendered.

This package has only been tested using a custom version of qualia:reval and the blaze-component package. It should support changes for any blaze template though.

This package must be added BEFORE any package that uses blaze (or templating).
