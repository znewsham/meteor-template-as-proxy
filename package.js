Package.describe({
  name: 'znewsham:template-as-proxy',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'WIP: use proxies for blaze templates',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md',
  debugOnly: true
});

Package.onUse(function(api) {
  api.versionsFrom('1.8');
  api.use(['ecmascript', 'blaze@2.3.3', 'templating@1.3.2'], 'client');
  api.mainModule('template-as-proxy.js', "client");
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:template-as-proxy');
  api.mainModule('template-as-proxy-tests.js');
});
